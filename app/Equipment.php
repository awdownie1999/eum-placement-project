<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{

    protected $table = 'equipments';


    protected $fillable = [
        'cost', 'years', 'description', 'make', 'X', 'Y', 'park_id', 'category_id', 'image',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    
    public function park()
    {
        return $this->belongsTo('App\Park');
    }

    public function sensor()
    {
        return $this->hasOne('App\Sensor');
    }

    public function getImage(){
        return $this->image;
    }

}
