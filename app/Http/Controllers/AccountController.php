<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Organisation;
use Illuminate\Support\Facades\Auth;
use App\User;

class AccountController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if($user->organisation_id != null) {
            $organisation = $user->organisation;
            Log::info('ID ==  ' . $user->organisation_id);
        }else {
            $organisation = null;
        }
        return view('Account.account', compact('organisation', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organisations = Organisation::all();
        $user = Auth::user();
        return view('Account.editAccount', compact('organisations', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'organisation_id' => 'required',
            'name' => 'required|string',
            'email' => 'required|string|min:8',
            'address' => 'required|string',
            'address_two' => 'nullable|string',
            'contact_num' => 'required|numeric|min:11',
            'contact_two' => 'nullable|numeric|min:11',
        ]);

        $account = User::find($id);
        $account->organisation_id = $request->get('organisation_id');
        $account->name = $request->get('name');
        Log::info($request->get('name'));
        $account->email = $request->get('email');
        $account->address = $request->get('address');
        $account->address_two = $request->get('address_two');
        $account->contact_num = $request->get('contact_num');
        $account->contact_two = $request->get('contact_two');

        $account->save();
        return redirect('/account')->with('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->organisation_id = null;
        $user->save();
        return redirect('/account')->with('organisation removed!');
    }
}