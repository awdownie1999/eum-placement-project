<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipment;
use Log;
use App\Park;
use App\Category;
use App\Sensor;
use Carbon\Traits\Timestamp;
use Intervention\Image\Facades\Image;


class EquipmentController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $equipments = Equipment::all();
        return view('Equipment.equipmentIndex', compact('equipments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $equipment = Equipment::all();
        $parks = Park::all();
        $categories = Category::all();
        return view('Equipment.addEquipment', compact('equipment', 'parks', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cost' => 'required|numeric',
            'years' => 'required|numeric',
            'description' => 'required|string|min:8',
            'make' => 'required|string',
            'X' => 'required|numeric',
            'Y' => 'required|numeric',
            'image' => 'nullable',
        ]);

        Log::info('Inital Log');
        if($request->has('image')){
            Log::info('Image check: '. $request->image);
            $file_name = $request->image->store('images');
            Log::info('File_name check: '. $file_name);
            $image_url = 'http://localhost:8000/storage/' . $file_name;
            
            $equipment = new Equipment([
                'cost' => $request->get('cost'),
                'years' => $request->get('years'),
                'description' => $request->get('description'),
                'make' => $request->get('make'),
                'X' => $request->get('X'),
                'Y' => $request->get('Y'),
                'image' => $image_url,
            ]);
        }else{
        
        $equipment = new Equipment([
            'cost' => $request->get('cost'),
            'years' => $request->get('years'),
            'description' => $request->get('description'),
            'make' => $request->get('make'),
            'X' => $request->get('X'),
            'Y' => $request->get('Y'),
        ]);
        }//else
        
        //Associate park & Category
        $park = Park::find($request->get('park_id'));
        $equipment->park()->associate($park);
        $category = Category::find($request->get('category_id'));
        Log::info('Category Log Association : ' . $category);
        $equipment->category()->associate($category);
        $equipment->save();

        //Create a Sensor Obj and assign to equipment 
        $sensor = new Sensor([
            'times_moved_hour' => 0,
            'equipment_id' => $equipment->id,
        ]);
        Log::info('Log check equipment ID : ' . $equipment->id);
        $equipmentID = $equipment->id;
        $sensor->equipment()->associate($equipmentID);
        Log::info('Sensor Log check object creation : ' . $sensor);
        Log::info('Log check sensor association : ' . $sensor->equipment_id);
        $sensor->save();
        return redirect('/equipments')->with('Equipment added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $equipment = Equipment::find($id);
        return view('Equipment.editEquipment', compact('equipment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cost' => 'required|numeric',
            'years' => 'required|numeric',
            'description' => 'required|string|min:8',
            'make' => 'required|string',
            'X' => 'required|numeric',
            'Y' => 'required|numeric',
            'image' => 'nullable',
        ]);
      
        $equipment = Equipment::find($id);
        if($request->has('image')){
            Log::info('Image Check: ' . $request->get('image'));
            $file_name = $request->image->store('images');
            $image_url = 'http://localhost:8000/storage/' . $file_name;
            $equipment->cost = $request->get('cost');
            $equipment->years = $request->get('years');
            $equipment->description = $request->get('description');
            $equipment->make = $request->get('make');
            $equipment->X = $request->get('X');
            $equipment->Y = $request->get('Y');
            $equipment->image = $image_url;
        }else {
            $equipment->cost = $request->get('cost');
            $equipment->years = $request->get('years');
            $equipment->description = $request->get('description');
            $equipment->make = $request->get('make');
            $equipment->X = $request->get('X');
            $equipment->Y = $request->get('Y');
        }
        $equipment->save();
        return redirect('/equipments')->with('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $equipment = Equipment::find($id);
        $equipment->delete();
        return redirect('/equipments')->with('equipment deleted!');
    }
}//class