<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Park;
use Log;
use App\User;
use App\Equipment;
use App\Sensor;


class ParkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $park = Park::all();
        return view('Park.indexPark', compact('park'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $park = Park::all();
        $users = User::where('level_of_access', 0)->get();
        return view('Park.addPark', compact('park', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $park = new Park([
            'name' => $request->get('name'),
            'location' => $request->get('location'),
        ]);
        Log::info('New Park Check : ' . $park);
        $user = User::find($request->get('user_id'));
        $park->user()->associate($user);
        $park->save();
        return redirect('/parks')->with('Park added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::where('level_of_access', 0)->get();
        $park = Park::find($id);
        return view('Park.editPark', compact('park', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info('Park ID Check : ' . $id);
        Log::info('User ID Check : ' . $request->get('user_id'));
        $park = Park::find($id);
        $park->location = $request->get('location');
        $park->name = $request->get('name');
        $park->user_id = $request->get('user_id');
        Log::info('User ID check Park assign' .$park->user_id);
        $park->save();
        return redirect('/parks')->with('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $park = Park::find($id);
        $park->delete();

        return redirect('/parks')->with('Park deleted!');
    }
}