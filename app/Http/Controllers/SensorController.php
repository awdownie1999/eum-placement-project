<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipment;
use Log;
use App\Park;
use App\Sensor;

class SensorController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //vars
        $parks = Park::all();
        $equipments = Equipment::with('park')->paginate(10);
        $filter = null;
        $search = false;
            
        if($request->has('query')){
            //if search bar is used
            $search = true;
            $equipmentsArray[] = array();
            $query = $request->get( 'query' );
            $parks = Park::where ( 'name', 'LIKE', '%' . $query . '%' )->orderBy('updated_at', 'DESC')->get();

                foreach($parks as $park){
                    $equipment = Equipment::where('park_id', $park->id)->get();
                    array_push($equipmentsArray, $equipment);
                    Log::info('Equipments Array: ' . $equipment);
                }//foreach
                Log::info('Check if query matches parks: ' . $parks);
            if (count ( $parks ) > 0) {
                return view ( 'Dash.dash', compact('filter', 'search', 'query', 'equipment', 'parks', 'equipments'));
            }else{
                $filter = 'NA';
                $search = false;
                return view( 'Dash.dash', compact('filter', 'search', 'equipment', 'equipments'));
            }//else 
            
        }else {
                // Use filters if search bar isn't used 
            if($request->get('display')  == 'total'||$request->get('display')  == null) {
                //display all equipment
                $filter = 'total';
                return view('Dash.dash', compact('parks', 'equipments', 'filter', 'search'));

            }else if ($request->get('display') == 'recent'){
                $parks = Park::all();
                $sensorData = Sensor::where('times_moved_hour', '>', 1)->get('equipment_id');
                Log::info('Sensor ID Past Hour Sensor moved > 0 : ' . $sensorData);
                //Assign variable for each row in sensorData array if not null
                if(empty($sensorData)){    
                    Log::info('Equipments All Empty Check :' . $equipments);
                    return view('Dash.dash', compact('parks', 'equipments', 'filter', 'search')); 
                }else{
                    $equipmentArray[] = array();    
                    foreach ($sensorData as $data) {
                        Log::info('Check SensorData For Equipmemt_id : ' . $data);
                        $equipments = Equipment::where('id', $data->equipment_id)->orderBy('updated_at', 'DESC')->paginate(5);
                        array_push($equipmentArray, $equipments);
                        Log::info('blasdfa');   
                    }//forEach
                    $equipments = $equipmentArray;
                $filter = 'recent';
                return view('Dash.dash', compact('parks', 'equipments', 'filter', 'search')); 
                }//else
            }//else
        }//else
    }//index

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}