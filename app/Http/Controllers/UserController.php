<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Organisation;
use Log;
use Hash;


class UserController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $users = User::all();
        foreach($users as $user) {
            $data = $user->organisation; 
            Log::info($data);
        }
        return view('User.userIndex', compact('users', 'data'));
    }
 
 public function create()
    {
        $users = User::all();
        $organisations = Organisation::all();

        return View('User.addUser', compact('users', 'organisations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'password' => 'required|string|min:8',
            'address' => 'required|string|min:10',
            'address_two' => 'nullable|string',
            'contact_num' => 'required|numeric|min:11',
            'conatct_two' => 'nullable|string',
        ]);

        $user = User::firstOrNew([
            'email' => $request['email'],
        ]);
        
        Log::info('Check' . $request->organisation_id);
        if($request->get('organisation_id') == 2) {
            $acessLevel = 1;
        } else {
            $acessLevel = 0;
        }
        $user->fill([
            'name' => $request->name,
            'password' => Hash::make($request->password),
            'address' => $request->address,
            'address_two' => $request->address_two,
            'contact_num' => $request->contact_num,
            'contact_two' => $request->contact_two,
            'organisation_id' => $request->organisation_id,
            'level_of_access' => $acessLevel,
            ]);
            
        $user->save();
        return redirect('/users')->with('User added');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organisations = Organisation::all();
        $user = User::find($id);
        return view('User.editUser', compact('organisations', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'password' => 'required|string|min:8',
            'address' => 'required|string|min:10',
            'address_two' => 'nullable|string',
            'conatct_num' => 'required|numeric|min:11',
            'conatct_two' => 'nullable|string|min:11',
        ]);

        if($request->get('organisation_id') == 2) {
            $acessLevel = 1;
        } else {
            $acessLevel = 0;
        }

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->address = $request->get('address');
        $user->address_two = $request->get('address_two');
        $user->contact_num = $request->get('contact_num');
        $user->contact_two = $request->get('contact_two');
        $user->organisation_id = $request->get('organisation_id');
        $user->level_of_access = $acessLevel;

        $user->save();
        return redirect('/users')->with('updated user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $suer = User::find($id);
        $suer->delete();

        return redirect('/users')->with('User deleted!');
    }
}
