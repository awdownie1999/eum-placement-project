<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    protected $fillable = [
        'name', 'type'
    ];

    public function user() {
        return $this->hasMany('App\User');
    }

}
