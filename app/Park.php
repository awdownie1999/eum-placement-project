<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Park extends Model
{
    protected $fillable = [
        'location', 'user_id', 'name',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }

    protected function equipment() {
        return $this->hasmany('App\Equipment');
    }
}
