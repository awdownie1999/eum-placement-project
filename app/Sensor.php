<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sensor extends Model
{
    protected $fillable = [
        'times_moved_hour'
    ];

    public function equipment() {
        return $this->belongsTo('App\Equipment');
    }
}
