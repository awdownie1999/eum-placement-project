<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            OrganisationsTableSeeder::class,
            UsersTableSeeder::class,
            ParksTableSeeder::class,
            CategoriesTableSeeder::class,
            EquipmentsTableSeeder::class,
            SensorsTableSeeder::class,
           
        ]);
    }
}
