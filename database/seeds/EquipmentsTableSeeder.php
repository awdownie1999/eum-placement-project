<?php

use Illuminate\Database\Seeder;
use App\Equipment;

class EquipmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $equipment = Equipment::firstOrNew([
            'cost'=> '400',
            'years'=> '3',
            'description'=> 'Lat Pull Down Bench',
            'make'=> 'Dijaio',
            'X'=> '54',
            'Y'=> '102',
            'park_id' => '1',
            'category_id' => '1'
        ]);
        $equipment->save();
    }
}
