<?php

use Illuminate\Database\Seeder;
use App\Organisation;

class OrganisationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organisation = Organisation::FirstOrNew([
            'name' => 'Belfast City',
            'type' => 'Council',
        ]);
        $organisation->save();

    }
}
