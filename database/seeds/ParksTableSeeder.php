<?php

use Illuminate\Database\Seeder;
use App\Park;

class ParksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $park = Park::firstOrNew([
            'location' => 'Park1',
            'user_id' => '1',
            'name' => 'Town Park'
        ]);

        $park->save();
    }
}
