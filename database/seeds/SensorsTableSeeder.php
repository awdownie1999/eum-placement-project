<?php

use Illuminate\Database\Seeder;
use App\Sensor;

class SensorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sensor = Sensor::firstOrNew([
            'times_moved_hour' => '40',
            'times_moved_total' => '400',
            'equipment_id'=> '1'
        ]);
        $sensor->save();
    }
}
