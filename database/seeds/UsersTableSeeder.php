<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //User
        $user = User::firstOrNew([
            'name' => 'Test User',
            'email' => 'test@example.com',
            'password' => Hash::make('password'),
            'address' => '43 User Road',
            'contact_num' => '06286348762',
            'level_of_access' => 0,
            'organisation_id' => 1

        ]);
            $user->save();


        //Admin
        $admin = User::firstOrNew([
            'name' => 'Admin User',
            'email' => 'admin@example.com',
            'password' => Hash::make('password'),
            'address' => '1 Admin Road',
            'contact_num' => '06286348762',
            'level_of_access' => 1,
            ]);
            $admin->save();
    }
}
