@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Account</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif

    <table style="width: 100%;">
        <tr>
            <td style="text-align: left;">
                <ul style="list-style-type: none;">

                    <li>{{ __('Name') }} : {{ Auth::user()->name }}</li>
                    <li>{{ __('Email') }} : {{ Auth::user()->email }}</li>
                    <li>{{ __('Address') }} : {{ Auth::user()->address }}</li>
                    <li>{{ __('Second Address') }} : {{ Auth::user()->address_two }}</li>
                    <li>{{ __('Contact Number') }} : {{ Auth::user()->contact_num }}</li>
                    <li>{{ __('Second Contact Number') }} : {{ Auth::user()->contact_two }}</li>

                    @if(Auth::user()->level_of_access != 1)
                    @if($organisation == null)
                        <br/>
                        <h3>There is no Organisation assigned to this user, set one using the button below:</h3>
                        <a style="margin: 19px;" allign="left" class="btn btn-primary" href="{{ route('account.edit', $user->id)}}">Assign Organisation</a> 
                    @else
                        <li>{{__('Organisation')}} : {{$organisation->name}}</li>
                        <br />
                        <td><a style="margin: 19px;" class="btn btn-primary" href="{{ route('account.edit', $user->id)}}">Edit Account</a></td> 
                        
                        <td><form action="{{ route('account.destroy', $user->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Remove Organisation</button></form></td> 
                    @endif
                    @endif
                </ul>
            </td>
            <td style=" text-align: right;">
                <i class="fas fa-user-circle fa-7x"></i>
            </td>
        </tr>
    </table>
    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
</div>
</div>
@endsection