@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Account</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif

    <form method="post" action="{{ route('account.update', $user->id)}}">
        @method('PATCH')
        @csrf
        <div name="form-group">
            <label for="name">Organisation Name</label>
            <select name ="organisation_id">
                @foreach($organisations as $organisation)
                    <option value="{{$organisation->id}}">{{$organisation->name}}</option>
                    @endforeach
            </select>
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $user->name }}">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" class="form-control" name="email" value="{{ $user->email }}">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" class="form-control" name="password" value="{{ $user->password }}">
            </div>
            <div class="form-group">
                <label for="address">Address:</label>
                <input type="text" class="form-control" name="address" value="{{ $user->address }}">
            </div>
            <div class="form-group">
                <label for="address_two">Address 2:</label>
                <input type="text" class="form-control" name="address_two" value="{{ $user->address_two }}">
            </div>
            <div class="form-group">
                <label for="contact_num">Contact Number:</label>
                <input type="text" class="form-control" name="contact_num" value="{{ $user->contact_num }}">
            </div>
            <div class="form-group">
                <label for="contact_two">Contact 2:</label>
                <input type="text" class="form-control" name="contact_two" value="{{ $user->contact_two }}">
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </form>

    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
</div>
</div>
@endsection
