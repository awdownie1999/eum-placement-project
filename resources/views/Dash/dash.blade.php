@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Sensor Data</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif

    <form action="/search" method="POST" role="search">
    {{ csrf_field() }}
    <div class="input-group">
        <input type="text" class="form-control" name="query"
            placeholder="Search Sensor Data by Park"> <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
    </div>
    </form>

    <a style="margin: 19px;" href="{{ route('dash.index', ['display'=> 'total']) }}" class="btn btn-primary">All Sensor Data</a>
    <a style="margin: 19px;" href="{{ route('dash.index', ['display'=> 'recent']) }}" class="btn btn-primary">Recent Recordings</a>
    @if($filter == 'total')
    <table class="table table-striped">
        <thead>
            <tr>
                <td>Park Name</td>
                <td>Park Location</td>
                <td>Equipment Years in park</td>
                <td>Equipment Description</td>
                <td>Make</td>
                <td>X Co-ordinate</td>
                <td>Y Co-ordinate</td>
                <td>Times Moved in an hour</td>
                <td>Total Times Moved</td>
                <td>Time</td>
            </tr>
        </thead>
        <tbody>
        @foreach($equipments as $equipment)
            <tr>
                <td>{{$equipment->park->name}} </td>
                <td>{{$equipment->park->location}} </td>
                <td>{{$equipment->years}}  </td>
                <td>{{$equipment->description}} </td>
                <td>{{$equipment->make}}</td>
                <td>{{$equipment->X}}</td>
                <td>{{$equipment->Y}}</td>
                <td>{{$equipment->sensor->times_moved_hour}} </td>
                <td>{{$equipment->sensor->times_moved_total}} </td>
                <td>{{$equipment->sensor->created_at}}</td>
                <td><a href="{{$equipment->image}}"><img src="{{$equipment->image}}" height="84" width="84" alt="Equipment Image"></a></td>
            </tr>
        @endforeach
        </tbody>
        <div class="row">
            <div class="col-12 d-flex justify-content-center pt-4">
                {{$equipments->links()}}
            </div>
        </div>
    </table>
    
    @elseif($filter == 'recent') 
    <table class="table table-striped">
         <thead>
            <tr>
                <td>Park Name</td>
                <td>Park Location</td>
                <td>Equipment ID</td>
                <td>Equipment Years in park</td>
                <td>Equipment Description</td>
                <td>Make</td>
                <td>X Co-ordinate</td>
                <td>Y Co-ordinate</td>
                <td>Times Moved in an hour</td>
                <td>Time</td>
            </tr>
        </thead>
        <tbody>
        @foreach($equipments as $equipment)
        @foreach($equipment as $equip)
            <tr>    
                <td>{{$equip->park->name}} </td>
                <td>{{$equip->park->location}} </td>
                <td>{{$equip->id}}  </td>
                <td>{{$equip->years}}  </td>
                <td>{{$equip->description}} </td>
                <td>{{$equip->make}}</td>
                <td>{{$equip->X}}</td>
                <td>{{$equip->Y}}</td>
                <td>{{$equip->sensor->times_moved_hour}} </td>
                <td>{{$equip->sensor->created_at}}</td>
                <td><a href="{{$equip->image}}"><img src="{{$equip->image}}" height="84" width="84" alt="Equipment Image"></a></td>
            </tr>
        @endforeach
        @endforeach
        </tbody>
        <div class="row">
            <div class="col-12 d-flex justify-content-center pt-4">
            {{$equipment->links()}}
            </div>
        </div>
        </table>
        @endif

        @if($search == true)
        <div class="container">
        <h2>Search Results:</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Park Name</td>
                    <td>Park Location</td>
                    <td>Equipment ID</td>
                    <td>Equipment Years in park</td>
                    <td>Equipment Description</td>
                    <td>Make</td>
                    <td>X Co-ordinate</td>
                    <td>Y Co-ordinate</td>
                    <td>Times Moved in an hour</td>
                    <td>Times Moved Total</td>
                    <td>Time</td>
                </tr>
            </thead>
            <tbody>
                @foreach($equipment as $equip)
                <tr>
                    <td>{{$equip->park->name}}</td>
                    <td>{{$equip->park->location}} </td>
                    <td>{{$equip->id}}  </td>
                    <td>{{$equip->years}} </td>
                    <td>{{$equip->description}} </td>
                    <td>{{$equip->make}}  </td>
                    <td>{{$equip->X}} </td>
                    <td>{{$equip->Y}} </td>
                    <td>{{$equip->sensor->times_moved_hour}} </td>
                    <td>{{$equip->sensor->times_moved_total}} </td>
                    <td>{{$equip->sensor->created_at}}</td>
                    <td><a href="{{$equip->image}}"><img src="{{$equip->image}}" height="84" width="84" alt="Equipment Image"></a></td>
                </tr>
                @endforeach
            </tbody>
            <div class="row">
                <div class="col-12 d-flex justify-content-center pt-4">
                {{$equipment->links()}}
                </div>
            </div>
        </table>
        
        </div>
        @endif

       @if ($filter == 'NA')
       <h1>No Search Results Found</h1>
       @endif
       
    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
    
</div>
</div>

@endsection