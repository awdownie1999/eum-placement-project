@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add Equipment</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->organisation != null|| Auth::user()->level_of_access == 1)
    <form method="post" action="{{ route('equipments.store') }}" enctype="multipart/form-data"> 
                @csrf
                <br/>
                <label for="image">Insert Image:</label>
                <br/>
                <input type="file" name="image" />
                <br/>
                <br/>
                <div class="form-group">
                    <label for="cost">cost:</label>
                    <input type="text" class="form-control" name="cost" />
                </div>
                <div class="form-group">
                <label for="category">Category:</label>
                <select name ="category_id">
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->type}}</option> 
                    @endforeach 
                </select></div>

                <div class="form-group">
                    <label for="years">years:</label>
                    <input type="text" class="form-control" name="years" />
                </div>
                <div class="form-group">
                    <label for="description">description:</label>
                    <input type="text" class="form-control" name="description" />
                </div>
                <div class="form-group">
                    <label for="make">make:</label>
                    <input type="text" class="form-control" name="make" />
                </div>
                <div class="form-group">
                    <label for="X">X:</label>
                    <input type="text" class="form-control" name="X" />
                </div>
                <div class="form-group">
                    <label for="Y">Y:</label>
                    <input type="text" class="form-control" name="Y" />
                </div>
                <div class="form-group">
                <label for="park">Park Location:</label>
                <select name ="park_id">
                    @foreach ($parks as $park)
                        <option value="{{$park->id}}">{{$park->name}}</option> 
                    @endforeach 
                </select></div>

                
                <button type="submit" class="btn btn-secondary">Add Equipment</button>
            </form>
            @else
            <h1>Please Select an organisation </h1>
            @endif
        </div>
    </div>
</div>
@endsection





