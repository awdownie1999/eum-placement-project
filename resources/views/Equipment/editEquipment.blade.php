@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Equipment</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->organisation != null|| Auth::user()->level_of_access == 1)
    <form method="post" action="{{ route('equipments.update', $equipment->id)}}" enctype="multipart/form-data">
        @method('PATCH')
        @csrf
            <br/>
            <label for="image">Update Image:</label>
            <br/>
            <input type="file" name="image" />
            <br/>
            <br/>
            <div class="form-group">
                <label for="cost">cost:</label>
                <input type="text" class="form-control" name="cost" value="{{ $equipment->cost }}">
            </div>
            <div class="form-group">
                <label for="years">years:</label>
                <input type="text" class="form-control" name="years" value="{{ $equipment->years }}">
            </div>
            <div class="form-group">
                <label for="make">Make:</label>
                <input type="text" class="form-control" name="make" value="{{ $equipment->make }}">
            </div>
            <div class="form-group">
                <label for="description">description:</label>
                <input type="text" class="form-control" name="description" value="{{ $equipment->description }}">
            </div>
            <div class="form-group">
                <label for="X">X :</label>
                <input type="text" class="form-control" name="X" value="{{ $equipment->X }}">
            </div>
            <div class="form-group">
                <label for="Y">Y:</label>
                <input type="text" class="form-control" name="Y" value="{{ $equipment->Y }}">
            </div>
            <button type="submit" href="" class="btn btn-primary">Update</button>
        </form>
        @else
        <h1>Please Select an organisation </h1>
        @endif
    </div>
</div>
@endsection
