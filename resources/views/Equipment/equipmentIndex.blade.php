@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Equipment</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->organisation != null|| Auth::user()->level_of_access == 1)
    <table class="table table-striped">
    <thead>
        <tr>
          <td>Equipment Image</td>
            <td>Cost</td>
            <td>Years</td>
            <td>Description</td>
            <td>Make</td>
            <td>X</td>
            <td>Y</td>
            <td>Related Park</td>
            <td>Category</td>
        </tr>
    </thead>
    <div>
        <a style="margin: 19px;" href="{{ route('equipments.create')}}" class="btn btn-primary">New Equipment</a>
    </div> 
    <tbody>
        @foreach($equipments as $equipment)
        <tr>

            <td><a href="{{$equipment->image}}"><img src="{{$equipment->image}}" height="84" width="84" alt="Equipment Image"></a></td>
            <td>{{$equipment->cost}} </td>
            <td>{{$equipment->years}} </td>
            <td>{{$equipment->description}} </td>
            <td>{{$equipment->make}} </td>
            <td>{{$equipment->X}} </td>
            <td>{{$equipment->Y}} </td>
            <td>{{$equipment->park->name}} </td>
            <td>{{$equipment->category->type}} </td>
            <td><a href="{{route('equipments.edit', $equipment->id)}}" class="btn btn-primary">Edit</a></td>
            <td><form action="{{ route('equipments.destroy', $equipment->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger" type="submit">Delete</button></form></td>
        </tr>
        @endforeach
    </tbody>
    </table>
    @else
    <h1>Please select an organisation</h1>
    @endif

    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
</div>
</div>
@endsection