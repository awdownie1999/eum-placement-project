@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Organisation</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->level_of_access != 0)
    <form method="post" action="{{ route('organisations.store') }}">
        @csrf
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" />
        </div>

        <div class="form-group">
            <label for="type">Type:</label>
            <input type="text" class="form-control" name="type" />
        </div>

        <button type="submit" class="btn btn-secondary">Add Organisation</button>
    </form>
    @endif
   </div>
 </div>
</div>
@endsection
