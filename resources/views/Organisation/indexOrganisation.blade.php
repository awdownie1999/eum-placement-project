@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Organisations</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->level_of_access != 0)
    <table class="table table-striped">
    <thead>
      <tr>
          <td>Name</td>
          <td>Type</td>
      </tr>
    </thead>
    <div>
        <a style="margin: 19px;" href="{{ route('organisations.create')}}" class="btn btn-primary">New Organisation</a>
    </div> 
    <tbody>
      @foreach($organisations as $organisation)
        <tr>
          <td>{{$organisation->name}} </td>
          <td>{{$organisation->type}} </td>
          <td><a href="{{route('organisations.edit', $organisation->id)}}" class="btn btn-primary">Edit</a></td>
          <td><form action="{{ route('organisations.destroy', $organisation->id)}}" method="post">
          @csrf
          @method('DELETE')
          <button class="btn btn-danger" type="submit">Delete</button></form></td>
        </tr>
      @endforeach
   </tbody>
</table>
@else
    <h1> You do not have permissions to access this page </h1>
    @endif
    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
 </div>
</div>
@endsection