@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Parks</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->organisation != null|| Auth::user()->level_of_access == 1)
    <form method="post" action="{{ route('parks.store') }}">
        @csrf
        <div class="form-group">
            <label for="location">Location:</label>
            <input type="text" class="form-control" name="location" />
        </div>

        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name" />
        </div>

        <div class="form-group">
          <label for="user_id">Park Manager:</label>
          <select name ="user_id">
          @foreach ($users as $user)
            <option value="{{$user->id}}">{{$user->name}}</option> 
          @endforeach 
          </select></div>
        <button type="submit" href="{{ route('parks.store', ['user_id'=>$user->id]) }}" class="btn btn-secondary">Add Park</button>
    </form>
    @else
    <h1>Please Select an organisation </h1>
    @endif
   </div>
 </div>
</div>
@endsection
