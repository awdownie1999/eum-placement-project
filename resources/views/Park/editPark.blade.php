@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit Parks</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->organisation != null|| Auth::user()->level_of_access == 1)
    <form method="post" action="{{ route('parks.update', $park->id)}}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="park">Park Name:</label>
                <input type="text" class="form-control" name="name" value="{{ $park->name }}">
            </div>
            <div class="form-group">
                <label for="location">Location:</label>
                <input type="text" class="form-control" name="location" value="{{ $park->location }}">
            </div>
            <div class="form-group">
                <label for="user_id">Park Manager</label>
                <select name="user_id">
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" href="" class="btn btn-primary">Update</button>
        </form>
        @else
        <h1>Please Select an organisation </h1>
        @endif
    </div>
</div>
@endsection
