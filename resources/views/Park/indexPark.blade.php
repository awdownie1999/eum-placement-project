@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Parks</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->organisation != null|| Auth::user()->level_of_access == 1)
    <table class="table table-striped">
    <thead>
        <tr>
            <td>Location</td>
            <td>Park Name</td>
            <td>Park Manager</td>
        </tr>
    </thead>
    <div>
        <a style="margin: 19px;" href="{{ route('parks.create')}}" class="btn btn-primary">New park</a>
    </div> 
    <tbody>
      
        @foreach($park as $park)
        <tr>
            <td>{{$park->location}} </td>
            <td>{{$park->name}} </td>
            <td>{{$park->user->name}} </td>
            <td><a href="{{route('parks.edit', $park->id)}}" class="btn btn-primary">Edit</a></td>
            <td><form action="{{ route('parks.destroy', $park->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger" type="submit">Delete</button></form></td>
        </tr>
        @endforeach
    
    </tbody>
    </table>
    @else
    <h1>Please select an organisation</h1>
    @endif


    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
</div>
</div>
@endsection