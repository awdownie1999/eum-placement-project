@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add User</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->level_of_access != 0)
    <form method="post" action="{{ route('users.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name">name:</label>
                    <input type="text" class="form-control" name="name" />
                </div>
                <div class="form-group">
                    <label for="email">email:</label>
                    <input type="text" class="form-control" name="email" />
                </div>
                <div class="form-group">
                    <label for="password">password:</label>
                    <input type="password" class="form-control" name="password" />
                </div>
                <div class="form-group">
                    <label for="address">address:</label>
                    <input type="text" class="form-control" name="address" />
                </div>
                <div class="form-group">
                    <label for="address_two">address 2:</label>
                    <input type="text" class="form-control" name="address_two" />
                </div>
                <div class="form-group">
                    <label for="contact_num">contact num:</label>
                    <input type="text" class="form-control" name="contact_num" />
                </div>
                <div class="form-group">
                    <label for="contact_two">contact two:</label>
                    <input type="text" class="form-control" name="contact_two" />
                </div>
                <button type="submit" class="btn btn-secondary">Add User</button>
            </form>
            @endif
        </div>
    </div>
</div>
@endsection





