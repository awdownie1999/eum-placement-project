@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit User</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->level_of_access != 0)
    <form method="post" action="{{ route('users.update', $user->id)}}">
            @method('PATCH')
            @csrf
            <div class="form-group">
                <label for="name">name:</label>
                <input type="text" class="form-control" name="name" value="{{ $user->name }}">
            </div>
            <div class="form-group">
                <label for="email">email:</label>
                <input type="text" class="form-control" name="email" value="{{ $user->email }}">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" class="form-control" name="password" value="{{ $user->password }}">
            </div>
            <div class="form-group">
                <label for="address">address:</label>
                <input type="text" class="form-control" name="address" value="{{ $user->address }}">
            </div>
            <div class="form-group">
                <label for="address_two">address_two :</label>
                <input type="text" class="form-control" name="address_two" value="{{ $user->address_two }}">
            </div>
            <div class="form-group">
                <label for="contact_num">contact_num :</label>
                <input type="text" class="form-control" name="contact_num" value="{{ $user->contact_num }}">
            </div>
            <div class="form-group">
                <label for="contact_two">contact_two:</label>
                <input type="text" class="form-control" name="contact_two" value="{{ $user->contact_two }}">
            </div>
            <div class="form-group">
                <label for="organisation_id">Organisation Name:</label>
                <select name ="organisation_id">
                    @foreach ($organisations as $organisation)
                        <option value="{{$organisation->id}}">{{$organisation->name}}</option> 
                    @endforeach 
                </select>
            </div>
            <button type="submit" href="" class="btn btn-primary">Update</button>
        </form>
        @endif
    </div>
</div>
@endsection
