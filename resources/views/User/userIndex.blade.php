@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Users</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    @if(Auth::user()->level_of_access != 0)
    <table class="table table-striped">
    <thead>
        <tr>
            <td>Name</td>
            <td>Email</td>
            <td>Address</td>
            <td>Address 2</td>
            <td>Contact Number</td>
            <td>Contact 2</td>
            <td>Level of Access</td>
            <td>Organisation</td>
        </tr>
    </thead>
    
    <div>
        <a style="margin: 19px;" href="{{ route('users.create')}}" class="btn btn-primary">New User</a>
    </div> 
    <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->name}} </td>
            <td>{{$user->email}} </td>
            <td>{{$user->address}} </td>
            <td>{{$user->address_two}} </td>
            <td>{{$user->contact_num}} </td>
            <td>{{$user->contact_two}} </td>
            <td>{{$user->level_of_access}} </td>
            @if($user->organisation != null)
            <td>{{$user->organisation->name}} </td>
            @else 
            <td>No Organisation</td>
            @endif
            <td><a href="{{route('users.edit', $user->id)}}" class="btn btn-primary">Edit</a></td>
            <td><form action="{{ route('users.destroy', $user->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger" type="submit">Delete</button></form></td>
        </tr>
        @endforeach
    </tbody>
    </table>
    @else
    <h1> You do not have permissions to access this page </h1>
    @endif
    @if(session()->get('success'))
        <div class="alert alert-success">
        {{ session()->get('success') }}  
        </div>
    @endif
</div>
</div>
@endsection