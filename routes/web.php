<?php
use App\Park;
use Illuminate\Support\Facades\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('parks', 'ParkController');
    Route::resource('account', 'AccountController');
    Route::resource('dash', 'SensorController');
    Route::resource('equipments', 'EquipmentController');
    Route::resource('organisations', 'OrganisationController');
    Route::resource('categories', 'CategoryController');
    Route::resource('users', 'UserController');
});
    
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get ( '/search', 'SensorController@index')->name('search');